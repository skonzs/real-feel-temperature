package com.example.realfeeltemperature;

public class RealFeelTemperature {
    private double humidity;
    private double windSpeed;
    private double temperature;

    public RealFeelTemperature(double humidity, double windSpeed, double temperature) {
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.temperature = temperature;
    }

    public double getHumidity() {
        return humidity;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }
    public double getRealFeelTemperature()
    {
        return temperature+(0.348*getWaterVapourPressure())-(0.70*windSpeed)-4.25;
    }
    private double getWaterVapourPressure()
    {
        return (humidity/100)*6.105*Math.exp(((17.27*temperature)/(237.7+temperature)));
    }

}

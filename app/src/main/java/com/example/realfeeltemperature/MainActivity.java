package com.example.realfeeltemperature;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private AppCompatTextView tvResult;
    private AppCompatEditText etWindSpeed;
    private AppCompatEditText etHumidity;
    private AppCompatEditText etTemperature;
    private AppCompatButton btnCalculate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
    }
    private void initComponents()
    {
        etWindSpeed=(AppCompatEditText)findViewById(R.id.etWindSpeed);
        etHumidity=(AppCompatEditText)findViewById(R.id.etHumidity);
        etTemperature=(AppCompatEditText)findViewById(R.id.etTemperature);
        tvResult=(AppCompatTextView)findViewById(R.id.tvResult);
        btnCalculate=(AppCompatButton)findViewById(R.id.btnCalculate);
        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double temperature;
                double windSpeed;
                double humidity;
                if(isValid()){
                    try {
                       humidity= Double.parseDouble(etHumidity.getText().toString());
                       windSpeed= Double.parseDouble(etWindSpeed.getText().toString());
                       temperature=  Double.parseDouble(etTemperature.getText().toString());
                       RealFeelTemperature realFeelTemperature =new RealFeelTemperature(humidity,windSpeed,temperature);
                       tvResult.setText( String.format("%.2f",realFeelTemperature.getRealFeelTemperature())+(char) 0x00B0+"C");
                    }catch (NumberFormatException e) {

                    }
                    catch (Exception e){

                    }

                }
            }
        });
    }
    private boolean isValid()
    {
        if (etTemperature.getText().toString().equals("")||etWindSpeed.getText().toString().equals("")||etHumidity.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "", Toast.LENGTH_LONG).show();
            return false;
        }else
            return true;
    }
}
